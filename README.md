# Tmux autoattach for [packer.nu][]

This auto attaches TMUX to new terminals.

**Note:** A more advanced version of this for nupm is available at <https://github.com/amtoine/tmux-sessionizer>

[packer.nu]: https://github.com/jan9103/packer.nu
